const fs = require('fs');

function createSortAndDeleteFiles(callback) {
    fs.readFile('./lipsum.txt', 'utf-8', (err, lipsumContent) => {
        if (err) {
            console.log(err);
            return;
        } else {

            lipsumContent = lipsumContent.toUpperCase();
            let upperCaseFile = "upperCaseFile.txt";
            callback(err,upperCaseFile + " created");
            fs.writeFile(upperCaseFile, lipsumContent, (err) => {
                if (err) {
                    console.log(err);
                    return;
                } else {
                    callback(err, `Write in ${upperCaseFile}`);
                    fs.writeFile('./filenames.txt', upperCaseFile, (err) => {
                        if (err) {
                            console.log(err);
                            return;
                        } else {
                            callback(err,`Inserted filename in filenames.txt`);
                            fs.readFile(upperCaseFile, 'utf-8', (err, upperCaseData) => {
                                if (err) {
                                    console.log(err);
                                    return;
                                } else {
                                    callback(err,upperCaseFile+' created');
                                    upperCaseData = upperCaseData.toLowerCase();
                                    upperCaseData = upperCaseData.split('.');
                                    let sentenceFile = "sentenceFile.txt";
                                    upperCaseData.forEach(sentence => {
                                        fs.appendFile(sentenceFile, sentence + '\n', (err) => {
                                            if (err) {
                                                console.log(err);
                                                return;
                                            }
                                            callback(err,`Adding in sentence file ...`);
                                        })
                                    })

                                    fs.appendFile('./filenames.txt', '\n' + sentenceFile, (err) => {
                                        if (err) {
                                            console.log(err);
                                            return;
                                        } else {
                                            callback(err,`Added ${sentenceFile} to filenames.txt`);
                                            fs.readFile('./filenames.txt', 'utf-8', (err, filesData) => {
                                                if (err) {
                                                    console.log(err)
                                                    return;
                                                } else {
                                                    callback(err, `files Data are ${filesData}`)
                                                    filesData = filesData.split('\n');
                                                    filesData.forEach(file => {
                                                        fs.readFile(file, 'utf-8', (err, content) => {
                                                            if (err) {
                                                                console.log(err)
                                                            } else {

                                                                content = content.split(' ');
                                                                content.sort((first, second) => {
                                                                    if (first > second) {
                                                                        return 1;
                                                                    } else if (first < second) {
                                                                        return -1;
                                                                    }
                                                                    else {
                                                                        return 0;
                                                                    }

                                                                })
                                                                content = content.toString();
                                                                sortedFile = 'sorted_' + file;
                                                                fs.writeFile(sortedFile, content, (err) => {
                                                                    if (err) {
                                                                        console.log(err);
                                                                        return;
                                                                    }
                                                                })
                                                                callback(err,`created ${sortedFile} successfully`);
                                                                fs.appendFile('filenames.txt', '\n' + sortedFile, (err) => {
                                                                    if (err) {
                                                                        console.log(err);
                                                                        return;
                                                                    } else {

                                                                        fs.readFile('./filenames.txt', 'utf-8', (err, filesData) => {
                                                                            if (err) {
                                                                                console.log('err');
                                                                                return;
                                                                            } else {

                                                                                filesData = filesData.split('\n');
                                                                                filesData.forEach(file => {
                                                                                    fs.unlink(file, err => {
                                                                                        if (err) {
                                                                                            console.log("file deleted Already");
                                                                                        } else {
                                                                                            callback(err,"file deleted successfully");
                                                                                        }
                                                                                    })
                                                                                })
                                                                            }
                                                                        })
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

module.exports = createSortAndDeleteFiles;