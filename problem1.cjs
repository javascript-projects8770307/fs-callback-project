const fs = require('fs');

function createAndDeleteFiles(callback) {
    fs.mkdir('jsonDirectory', (err) => {
        if (err) {
            console.log(err);
            return;
        } else {
            callback(err, 'Directory Created Successfully');
            let numberOfFiles = parseInt(Math.random() * 20);
            const filesArray = Array.from(Array(numberOfFiles).keys())
            filesArray.forEach(file => {
                fs.writeFile('./jsonDirectory/' + file + '.json', '', (err) => {
                    if (err) {
                        console.log(err);
                        return;
                    } else {
                        callback(err, `${file}.json created successfully`);
                    }
                })

            })

        }
        deleteFiles(callback);
    })
}
function deleteFiles(callback) {

    fs.readdir('jsonDirectory', (err, filesData) => {
        if (err) {
            console.log(err);
        } else {
            filesData.forEach(file => {
                fs.unlink('jsonDirectory/' + file, (err) => {
                    if (err) {
                        console.log(err);
                        return;
                    } else {
                        callback(err, `${file} is deleted`);
                    }
                })
            })
        }
    })
}


module.exports = createAndDeleteFiles ;